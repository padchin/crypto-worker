package run

import (
	"context"
	"gitlab.com/padchin/crypto_worker/config"
	"gitlab.com/padchin/crypto_worker/internal/db"
	"gitlab.com/padchin/crypto_worker/internal/infrastructure/component"
	"gitlab.com/padchin/crypto_worker/internal/infrastructure/db/migrate"
	"gitlab.com/padchin/crypto_worker/internal/infrastructure/db/scanner"
	"gitlab.com/padchin/crypto_worker/internal/infrastructure/errors"
	"gitlab.com/padchin/crypto_worker/internal/infrastructure/server"
	"gitlab.com/padchin/crypto_worker/internal/infrastructure/worker"
	"gitlab.com/padchin/crypto_worker/internal/models"
	"gitlab.com/padchin/crypto_worker/internal/modules"
	crypto_server "gitlab.com/padchin/crypto_worker/internal/modules/crypto_worker/server"
	"gitlab.com/padchin/crypto_worker/internal/storages"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	Sig      chan os.Signal
	Storages *storages.Storage
	Services *modules.Services
	worker   *worker.CryptoWorker
	gRPC     server.Server
	rabbit   *server.RabbitMQServer
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt received", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	//запускаем crypto worker
	errGroup.Go(func() error {
		err := a.worker.Work(ctx)
		if err != nil {
			a.logger.Error("crypto worker: worker error", zap.Error(err))
			return err
		}
		return nil
	})

	errGroup.Go(func() error {
		err := a.gRPC.Serve(context.Background())
		if err != nil {
			a.logger.Fatal("app: gRPC server error", zap.Error(err))
			return err
		}
		return nil
	})

	errGroup.Go(func() error {
		err := a.rabbit.Serve(context.Background())
		if err != nil {
			a.logger.Fatal("app: rabbit server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// инициализация компонентов
	components := component.NewComponents(a.conf, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.SymbolsHistoryData{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, _, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация хранилищ
	storage := storages.NewStorage(dbx)
	a.Storages = storage
	// инициализация сервисов

	a.rabbit = server.NewRabbitMQServer(&a.conf.RabbitMQ, a.logger)

	services := modules.NewServices(storage, components, a.rabbit)

	a.Services = services

	a.worker = worker.NewCryptoWorker(a.logger, services, &a.conf.Worker)

	// инициализация сервера gRPC
	s := grpc.NewServer()
	reflection.Register(s)
	crypto_server.RegisterCryptoServiceServer(s, crypto_server.NewCryptoServer(a.Services.CryptoService))

	a.gRPC = server.NewGRPC(&a.conf.RPCServer, s, a.logger)

	return a
}
