package config

import (
	"go.uber.org/zap"
	"os"
	"strconv"
	"time"
)

const (
	AppName = "APP_NAME"

	serverPort         = "SERVER_PORT"
	envShutdownTimeout = "SHUTDOWN_TIMEOUT"
	envAccessTTL       = "ACCESS_TTL"
	envRefreshTTL      = "REFRESH_TTL"
	envVerifyLinkTTL   = "VERIFY_LINK_TTL"

	parseShutdownTimeoutError    = "config: parse server shutdown timeout error"
	parseRpcShutdownTimeoutError = "config: parse rpc server shutdown timeout error"
	parseTokenTTlError           = "config: parse token ttl error"
)

//go:generate easytags $GOFILE yaml

type AppConf struct {
	AppName     string         `yaml:"app_name"`
	Environment string         `yaml:"environment"`
	Domain      string         `yaml:"domain"`
	APIUrl      string         `yaml:"api_url"`
	Server      Server         `yaml:"server"`
	Logger      Logger         `yaml:"logger"`
	DB          DB             `yaml:"db"`
	Worker      Worker         `yaml:"worker"`
	RPCServer   RPCServer      `yaml:"rpc_server"`
	RabbitMQ    RabbitMQConfig `yaml:"rabbit_mq"`
}

type RabbitMQConfig struct {
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

type Worker struct {
	DelayDuration time.Duration
}

type RPCServer struct {
	Port string `yaml:"port"`
}

type DB struct {
	Net      string `yaml:"net"`
	Driver   string `yaml:"driver"`
	Name     string `yaml:"name"`
	User     string `json:"-" yaml:"user"`
	Password string `json:"-" yaml:"password"`
	Host     string `yaml:"host"`
	MaxConn  int    `yaml:"max_conn"`
	Port     string `yaml:"port"`
	Timeout  int    `yaml:"timeout"`
}

type Logger struct {
	Level string `yaml:"level"`
}

func NewAppConf() AppConf {
	port := os.Getenv(serverPort)

	appConf := AppConf{
		AppName: os.Getenv(AppName),
		Server: Server{
			Port: port,
		},
		DB: DB{
			Net:      os.Getenv("DB_NET"),
			Driver:   os.Getenv("DB_DRIVER"),
			Name:     os.Getenv("DB_NAME"),
			User:     os.Getenv("DB_USER"),
			Password: os.Getenv("DB_PASSWORD"),
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
		},
		Worker:    Worker{DelayDuration: time.Second * 15},
		RPCServer: RPCServer{Port: os.Getenv("GRPC_PORT")},
		RabbitMQ: RabbitMQConfig{
			Port:     os.Getenv("RABBIT_PORT"),
			User:     os.Getenv("RABBIT_USER"),
			Password: os.Getenv("RABBIT_PASSWORD"),
		},
	}

	return appConf
}

func (a *AppConf) Init(logger *zap.Logger) {
	shutDownTimeOut, err := strconv.Atoi(os.Getenv(envShutdownTimeout))
	if err != nil {
		logger.Fatal(parseShutdownTimeoutError)
	}
	shutDownTimeout := time.Duration(shutDownTimeOut) * time.Second
	if err != nil {
		logger.Fatal(parseRpcShutdownTimeoutError)
	}

	dbTimeout, err := strconv.Atoi(os.Getenv("DB_TIMEOUT"))
	if err != nil {
		logger.Fatal("config: parse db timeout err", zap.Error(err))
	}
	dbMaxConn, err := strconv.Atoi(os.Getenv("MAX_CONN"))
	if err != nil {
		logger.Fatal("config: parse db max connection err", zap.Error(err))
	}
	a.DB.Timeout = dbTimeout
	a.DB.MaxConn = dbMaxConn

	a.Domain = os.Getenv("DOMAIN")
	a.APIUrl = os.Getenv("API_URL")

	a.Server.ShutdownTimeout = shutDownTimeout
}

type Server struct {
	Port            string        `yaml:"port"`
	ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
}
