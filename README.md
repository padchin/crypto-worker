# crypto_worker

Реализован gRPC сервер, поддерживающий следующие методы:

	GetMinPrice(context.Context, *GetMinPriceRequest) (*GetMinPriceResponse, error)
	GetMaxPrice(context.Context, *GetMaxPriceRequest) (*GetMaxPriceResponse, error)
	GetTopMinPrices(context.Context, *GetTopMinPricesRequest) (*GetTopMinPricesResponse, error)
	GetTopMaxPrices(context.Context, *GetTopMaxPricesRequest) (*GetTopMaxPricesResponse, error)
	AddSymbolsPrice(context.Context, *AddSymbolsPriceRequest) (*AddSymbolsPriceResponse, error)

Добавлен брокер сообщений RabbitMQ, отправляющий в очередь `crypto` сообщение о текущих ценах на все криптовалютные пары с биржи Bybit после каждого обновления информации о цене.

Ссылка на gateway_service: https://gitlab.com/padchin/gateway_service/-/tree/grpc?ref_type=heads