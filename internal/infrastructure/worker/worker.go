package worker

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/padchin/crypto_worker/config"
	"gitlab.com/padchin/crypto_worker/internal/modules"
	"gitlab.com/padchin/crypto_worker/internal/modules/crypto_worker/service"
	"go.uber.org/zap"
	"time"
)

type CryptoWorker struct {
	logger  *zap.Logger
	service *modules.Services
	config  *config.Worker
}

func NewCryptoWorker(logger *zap.Logger, service *modules.Services, config *config.Worker) *CryptoWorker {
	return &CryptoWorker{logger: logger, service: service, config: config}
}

func (s *CryptoWorker) Work(ctx context.Context) error {
	var err error

	chErr := make(chan error)

	s.logger.Info("crypto worker started")

	go s.worker(ctx)

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return err
}

func (s *CryptoWorker) worker(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			s.logger.Error("stopping worker")
			return
		default:
			tickers, err := s.service.CryptoService.GetTickers()
			if err != nil {
				s.logger.Error("error getting tickers", zap.Error(err))
				time.Sleep(s.config.DelayDuration)
				continue
			}
			s.logger.Info(fmt.Sprintf("tickers recieved: %d", len(tickers)))
			// Формируем map с данными о ценах всех валютных пар
			prices := make(map[string]float64)
			for _, ticker := range tickers {
				prices[ticker.Symbol] = ticker.LastPrice
			}

			// Вызываем метод добавления цен в базу данных
			err = s.service.CryptoService.AddSymbolsPrice(prices, time.Now())
			if err != nil {
				s.logger.Error("error adding prices", zap.Error(err))
			}
			s.logger.Info(fmt.Sprintf("tickers added: %d", len(tickers)))

			// publish message
			err = s.publishMessage(tickers)

			time.Sleep(s.config.DelayDuration)
		}
	}
}

func (s *CryptoWorker) publishMessage(tickersData []service.Ticker) error {
	// Сериализация map в JSON
	jsonData, err := json.Marshal(tickersData)
	if err != nil {
		s.logger.Error("serialization error", zap.Error(err))

		return err
	}

	err = s.service.RabbitMQService.PublishMessage("crypto", jsonData)
	if err != nil {
		s.logger.Error("error publishing", zap.Error(err))

		return err
	}

	return nil
}
