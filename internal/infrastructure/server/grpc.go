package server

import (
	"context"
	"fmt"
	"gitlab.com/padchin/crypto_worker/config"
	"go.uber.org/zap"
	"net"

	"google.golang.org/grpc"
)

type Server interface {
	Serve(ctx context.Context) error
}

type ServerGRPC struct {
	conf   *config.RPCServer
	logger *zap.Logger
	srv    *grpc.Server
}

func NewGRPC(conf *config.RPCServer, srv *grpc.Server, logger *zap.Logger) Server {
	return &ServerGRPC{conf: conf, logger: logger, srv: srv}
}

func (s *ServerGRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var lis net.Listener
		s.logger.Info(fmt.Sprintf("gRPC Server is listening on port %s", s.conf.Port))
		lis, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			s.logger.Error("gRPC server register error", zap.Error(err))
			chErr <- err
			return
		}

		err = s.srv.Serve(lis)
		if err != nil {
			s.logger.Error("gRPC server serve error", zap.Error(err))
			chErr <- err
			return
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return err
}
