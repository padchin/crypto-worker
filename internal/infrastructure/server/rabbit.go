package server

import (
	"context"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/padchin/crypto_worker/config"
	"go.uber.org/zap"
)

type RabbitMQServer struct {
	conf   *config.RabbitMQConfig
	logger *zap.Logger
	conn   *amqp.Connection
	ch     *amqp.Channel
}

func NewRabbitMQServer(conf *config.RabbitMQConfig, logger *zap.Logger) *RabbitMQServer {
	return &RabbitMQServer{conf: conf, logger: logger}
}

func (s *RabbitMQServer) Connect() error {
	var err error
	connectionString := fmt.Sprintf("amqp://guest:guest@crypto_worker-rabbitmq:%s/", s.conf.Port)
	s.conn, err = amqp.Dial(connectionString)
	if err != nil {
		s.logger.Error("RabbitMQ connection error", zap.Error(err))
		return err
	}

	s.ch, err = s.conn.Channel()
	if err != nil {
		s.logger.Error("RabbitMQ channel error", zap.Error(err))
		return err
	}

	// Тут можно объявить очереди и обменники, если это необходимо

	return nil
}

func (s *RabbitMQServer) Close() {
	if s.ch != nil {
		s.ch.Close()
	}
	if s.conn != nil {
		s.conn.Close()
	}
}

func (s *RabbitMQServer) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		err = s.Connect()
		if err != nil {
			chErr <- err
			return
		}

		s.logger.Info(fmt.Sprintf("RabbitMQ Server is connected"))

		// Объявляем очередь "crypto" (если она еще не существует)
		queueName := "crypto"
		_, err = s.ch.QueueDeclare(queueName, false, false, false, false, nil)
		if err != nil {
			chErr <- err
			return
		}

		// Тут можно начать прослушивание очередей и обработку сообщений

		<-ctx.Done()
		s.Close()
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return nil
}

func (s *RabbitMQServer) PublishMessage(queueName string, body []byte) error {
	if s.ch == nil {
		s.logger.Warn("rabbitmq is not yet connected")

		return nil
	}

	err := s.ch.Publish(
		"",        // обменник (пустая строка означает использование обменника по умолчанию)
		queueName, // имя очереди, куда отправляется сообщение
		false,     // обязательное подтверждение (mandatory)
		false,     // безопасное подтверждение (immediate)
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body, // содержимое сообщения
		},
	)
	if err != nil {
		s.logger.Error("Failed to publish a message", zap.Error(err))
		return err
	}

	return nil
}
