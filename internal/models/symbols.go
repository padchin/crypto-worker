package models

import "time"

type SymbolsHistoryData struct {
	ID        int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Symbol    string    `json:"symbol" db:"symbol" db_type:"varchar(50)" db_default:"not null" db_index:"index"`
	LastPrice float64   `json:"last_price" db:"last_price" db_type:"decimal(18,8)" db_default:"not null"`
	QueryTime time.Time `json:"query_time" db:"query_time" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func (s *SymbolsHistoryData) TableName() string {
	return "symbols_history_data"
}

func (s *SymbolsHistoryData) OnCreate() []string {
	return []string{}
}

type SymbolData struct {
	Symbol    string  `json:"symbol"`
	LastPrice float64 `json:"lastPrice"`
}
