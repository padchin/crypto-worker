package storage

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"strings"
	"time"
)

type CryptoStorage struct {
	db *sqlx.DB
}

func NewCryptoStorage(db *sqlx.DB) *CryptoStorage {
	return &CryptoStorage{db: db}
}

// GetAveragePrice возвращает среднюю цену по определенному символу за указанный период времени.
func (cs *CryptoStorage) GetAveragePrice(symbol string, startTime time.Time, endTime time.Time) (float64, error) {
	// SQL-запрос для получения средней цены по символу за указанный период времени
	query := `
		SELECT AVG(last_price) FROM symbols_history_data
		WHERE symbol = $1
		AND query_time >= $2 AND query_time <= $3
	`

	// Выполнение SQL-запроса и получение средней цены
	var avgPrice float64
	err := cs.db.QueryRow(query, symbol, startTime, endTime).Scan(&avgPrice)
	if err != nil {
		return 0, err
	}

	return avgPrice, nil
}

// GetMinPrice возвращает минимальную цену для указанного символа (монеты) за указанный период.
func (cs *CryptoStorage) GetMinPrice(symbol string, startTime time.Time, endTime time.Time) (float64, error) {
	// SQL-запрос для получения минимальной цены за указанный период
	query := `
		SELECT MIN(last_price) AS min_price
		FROM symbols_history_data
		WHERE symbol = $1
		AND query_time >= $2 AND query_time <= $3
	`

	// Выполнение SQL-запроса и получение результата
	var minPrice float64
	err := cs.db.QueryRow(query, symbol, startTime, endTime).Scan(&minPrice)
	if err != nil {
		return 0, err
	}

	return minPrice, nil
}

// GetMaxPrice возвращает максимальную цену для указанного символа (монеты) за указанный период.
func (cs *CryptoStorage) GetMaxPrice(symbol string, startTime time.Time, endTime time.Time) (float64, error) {
	// SQL-запрос для получения максимальной цены за указанный период
	query := `
		SELECT MAX(last_price) AS max_price
		FROM symbols_history_data
		WHERE symbol = $1
			AND query_time >= $2 AND query_time <= $3
	`

	// Выполнение SQL-запроса и получение результата
	var maxPrice float64
	err := cs.db.QueryRow(query, symbol, startTime, endTime).Scan(&maxPrice)
	if err != nil {
		return 0, err
	}

	return maxPrice, nil
}

func (cs *CryptoStorage) GetTopMinPrices(limit int) ([]string, error) {
	// SQL-запрос для получения списка криптовалют с наименьшими ценами из последней записи
	query := `
		SELECT symbol
		FROM symbols_history_data AS shd
		WHERE query_time = (
    	SELECT MIN(query_time)
    	FROM symbols_history_data
    	WHERE symbol = shd.symbol
		)
		ORDER BY last_price ASC
		LIMIT $1
	`

	// Выполнение SQL-запроса и получение результата
	var symbols []string
	rows, err := cs.db.Query(query, limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var symbol string
		err := rows.Scan(&symbol)
		if err != nil {
			return nil, err
		}
		symbols = append(symbols, symbol)
	}

	return symbols, nil
}

func (cs *CryptoStorage) GetTopMaxPrices(limit int) ([]string, error) {
	// SQL-запрос для получения списка криптовалют с наивысшими ценами из последней записи
	query := `
		SELECT symbol
		FROM symbols_history_data AS shd
		WHERE query_time = (
    	SELECT MAX(query_time)
    	FROM symbols_history_data
    	WHERE symbol = shd.symbol
		)
		ORDER BY last_price DESC
		LIMIT $1
	`

	// Выполнение SQL-запроса и получение результата
	var symbols []string
	rows, err := cs.db.Query(query, limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var symbol string
		err := rows.Scan(&symbol)
		if err != nil {
			return nil, err
		}
		symbols = append(symbols, symbol)
	}

	return symbols, nil
}

// AddSymbolsPrice добавляет новые цены для указанных символов (монет) в таблицу symbol_prices.
func (cs *CryptoStorage) AddSymbolsPrice(prices map[string]float64, queryTime time.Time) error {
	// Начало транзакции
	tx, err := cs.db.Begin()
	if err != nil {
		return err
	}

	// Подготовка данных для множественного INSERT
	var values []string
	for symbol, price := range prices {
		values = append(values, fmt.Sprintf("('%s', %f, '%s')", symbol, price, queryTime.Format("2006-01-02 15:04:05")))
	}

	// SQL-запрос для множественного INSERT
	query := fmt.Sprintf(`
		INSERT INTO symbols_history_data (symbol, last_price, query_time)
		VALUES %s
	`, strings.Join(values, ","))

	// Выполняем SQL-запрос внутри транзакции
	_, err = tx.Exec(query)
	if err != nil {
		// Если произошла ошибка, откатываем транзакцию
		_ = tx.Rollback()
		return err
	}

	// Если все операции прошли успешно, фиксируем транзакцию
	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}
