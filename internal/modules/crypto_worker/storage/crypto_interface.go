package storage

import "time"

type CryptoStorager interface {
	// GetMinPrice returns the minimum price of a specific cryptocurrency within the given period.
	GetMinPrice(symbol string, startTime time.Time, endTime time.Time) (float64, error)

	// GetMaxPrice returns the maximum price of a specific cryptocurrency within the given period.
	GetMaxPrice(symbol string, startTime time.Time, endTime time.Time) (float64, error)

	// GetTopMinPrices returns a list of top cryptocurrencies with the lowest prices from the last record.
	GetTopMinPrices(limit int) ([]string, error)

	// GetTopMaxPrices returns a list of top cryptocurrencies with the highest prices from the last record.
	GetTopMaxPrices(limit int) ([]string, error)

	// AddSymbolsPrice adds a new price for a specific cryptocurrency symbol to the storage.
	AddSymbolsPrice(prices map[string]float64, queryTime time.Time) error
}
