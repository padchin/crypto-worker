package server

import (
	"context"
	"gitlab.com/padchin/crypto_worker/internal/modules/crypto_worker/service"
)

type CryptoServer struct {
	cryptoService *service.CryptoService
}

func NewCryptoServer(cryptoService *service.CryptoService) *CryptoServer {
	return &CryptoServer{cryptoService: cryptoService}
}

func (c *CryptoServer) GetMinPrice(ctx context.Context, request *GetMinPriceRequest) (*GetMinPriceResponse, error) {
	symbol := request.Symbol
	startTime := request.StartTime.AsTime()
	endTime := request.EndTime.AsTime()

	minPrice, err := c.cryptoService.GetMinPrice(symbol, startTime, endTime)
	if err != nil {
		return nil, err
	}

	response := &GetMinPriceResponse{
		MinPrice: minPrice,
	}

	return response, nil
}

func (c *CryptoServer) GetMaxPrice(ctx context.Context, request *GetMaxPriceRequest) (*GetMaxPriceResponse, error) {
	symbol := request.Symbol
	startTime := request.StartTime.AsTime()
	endTime := request.EndTime.AsTime()

	maxPrice, err := c.cryptoService.GetMaxPrice(symbol, startTime, endTime)
	if err != nil {
		return nil, err
	}

	response := &GetMaxPriceResponse{
		MaxPrice: maxPrice,
	}

	return response, nil
}

func (c *CryptoServer) GetTopMinPrices(ctx context.Context, request *GetTopMinPricesRequest) (*GetTopMinPricesResponse, error) {
	limit := int(request.Limit)

	topMinPrices, err := c.cryptoService.GetTopMinPrices(limit)
	if err != nil {
		return nil, err
	}

	response := &GetTopMinPricesResponse{
		TopMinPrices: topMinPrices,
	}

	return response, nil
}

func (c *CryptoServer) GetTopMaxPrices(ctx context.Context, request *GetTopMaxPricesRequest) (*GetTopMaxPricesResponse, error) {
	limit := int(request.Limit)

	topMaxPrices, err := c.cryptoService.GetTopMaxPrices(limit)
	if err != nil {
		return nil, err
	}

	response := &GetTopMaxPricesResponse{
		TopMaxPrices: topMaxPrices,
	}

	return response, nil
}

func (c *CryptoServer) AddSymbolsPrice(ctx context.Context, request *AddSymbolsPriceRequest) (*AddSymbolsPriceResponse, error) {
	//
	return nil, nil
}

func (c CryptoServer) mustEmbedUnimplementedCryptoServiceServer() {
	// Nothing to implement here
}
