package service

import (
	"encoding/json"
	"fmt"
	"gitlab.com/padchin/crypto_worker/internal/modules/crypto_worker/storage"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

type CryptoService struct {
	storage *storage.CryptoStorage
}

func NewCryptoService(storage *storage.CryptoStorage) *CryptoService {
	return &CryptoService{storage: storage}
}

func (c *CryptoService) GetMinPrice(symbol string, startTime time.Time, endTime time.Time) (float64, error) {
	return c.storage.GetMinPrice(symbol, startTime, endTime)
}

func (c *CryptoService) GetMaxPrice(symbol string, startTime time.Time, endTime time.Time) (float64, error) {
	return c.storage.GetMaxPrice(symbol, startTime, endTime)
}

func (c *CryptoService) GetTopMinPrices(limit int) ([]string, error) {
	return c.storage.GetTopMinPrices(limit)
}

func (c *CryptoService) GetTopMaxPrices(limit int) ([]string, error) {
	return c.storage.GetTopMaxPrices(limit)
}

func (c *CryptoService) AddSymbolsPrice(prices map[string]float64, queryTime time.Time) error {
	err := c.storage.AddSymbolsPrice(prices, queryTime)
	if err != nil {
		return err
	}

	return nil
}

type Tickers struct {
	RetCode int    `json:"ret_code"`
	RetMsg  string `json:"ret_msg"`
	Result  []struct {
		Symbol    string `json:"symbol"`
		LastPrice string `json:"last_price"`
	} `json:"result"`
	ExtCode string `json:"ext_code"`
	ExtInfo string `json:"ext_info"`
	TimeNow string `json:"time_now"`
}

type Ticker struct {
	Symbol    string  `json:"symbol"`
	LastPrice float64 `json:"last_price"`
}

func (c *CryptoService) GetTickers() ([]Ticker, error) {
	url := "https://api.bybit.com/v2/public/tickers"

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to fetch data. Status: %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var tickers Tickers
	err = json.Unmarshal(body, &tickers)
	if err != nil {
		return nil, err
	}

	var result []Ticker
	for _, data := range tickers.Result {
		ticker := Ticker{
			Symbol:    data.Symbol,
			LastPrice: atof(data.LastPrice),
		}
		result = append(result, ticker)
	}

	return result, nil
}

func atof(s string) float64 {
	val, _ := strconv.ParseFloat(s, 64)
	return val
}
