package modules

import (
	"gitlab.com/padchin/crypto_worker/internal/infrastructure/component"
	is_server "gitlab.com/padchin/crypto_worker/internal/infrastructure/server"
	"gitlab.com/padchin/crypto_worker/internal/modules/crypto_worker/server"
	"gitlab.com/padchin/crypto_worker/internal/modules/crypto_worker/service"
	"gitlab.com/padchin/crypto_worker/internal/storages"
)

type Services struct {
	RabbitMQService  *is_server.RabbitMQServer
	CryptoService    *service.CryptoService
	CryptoServerGRPC *server.CryptoServer
	Components       *component.Components
}

func NewServices(storage *storages.Storage, components *component.Components, rabbitServer *is_server.RabbitMQServer) *Services {
	return &Services{
		RabbitMQService:  rabbitServer,
		CryptoService:    service.NewCryptoService(storage.Crypto),
		CryptoServerGRPC: &server.CryptoServer{},
		Components:       components,
	}
}
