package storages

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/padchin/crypto_worker/internal/modules/crypto_worker/storage"
)

type Storage struct {
	Crypto *storage.CryptoStorage
}

func NewStorage(dbx *sqlx.DB) *Storage {
	return &Storage{
		Crypto: storage.NewCryptoStorage(dbx),
	}
}
